package com.appetiser.module.common.extensions

import android.content.Context
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.NavDirections

// for preventing illegal state exception
fun NavController.safeNavigate(direction: NavDirections) {
    currentDestination?.getAction(direction.actionId)?.run {
        navigate(direction)
    }
}
// for showing toast
fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}