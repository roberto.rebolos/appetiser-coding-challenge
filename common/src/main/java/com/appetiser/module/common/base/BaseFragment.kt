package com.appetiser.module.common.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.appetiser.module.common.di.Injectable
import com.appetiser.module.common.utils.displayer.ImageDisplayer
import com.appetiser.module.common.utils.schedulers.BaseSchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Defining all the fields and methods that is the same with child fragments
 * @param VB- responsible for instantiating the viewbinding
 */
abstract class BaseFragment<VB : ViewBinding> : Fragment(), Injectable {
    lateinit var binding: VB
    protected val disposables: CompositeDisposable = CompositeDisposable()

    @Inject
    lateinit var imageDisplayer: ImageDisplayer

    @Inject
    lateinit var scheduler: BaseSchedulerProvider
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = getViewBinding()

        return binding.root
    }

    open fun isExitOnBack(): Boolean = false

    // this is to bind the sub activity
    abstract fun getViewBinding(): VB

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (isExitOnBack()) {
            val callback = object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    findNavController().popBackStack()
                }

            }
            requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
        }
    }


}