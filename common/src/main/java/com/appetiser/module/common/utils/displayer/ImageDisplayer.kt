package com.appetiser.module.common.utils.displayer

import android.app.Activity
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import javax.inject.Inject
import javax.inject.Named

/**
 * This class is responsible to load the image using glide
 */
class ImageDisplayer @Inject constructor() {

    /**
      @param albumImage- for image to be display of the image url coming from itunes API
      @param imageUrl- this is the image url coming from itunes API
     */
    fun displayImage(albumImage: ImageView, imageUrl: String) {
        Glide.with(albumImage)
            .load(imageUrl)
            .centerCrop()
            .thumbnail(0.5f)

            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(albumImage)


    }
}