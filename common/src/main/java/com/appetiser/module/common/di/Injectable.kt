package com.appetiser.module.common.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
