package com.appetiser.module.common.base

import android.os.Bundle
import android.util.Log
import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import com.appetiser.module.common.utils.schedulers.BaseSchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

abstract class BaseViewModel : ViewModel() {
    // to basically improve the performance
    open val disposables: CompositeDisposable by lazy {
        CompositeDisposable()
    }

    @Inject
    lateinit var schedulerProvider: BaseSchedulerProvider

    // to prevent having memory leaks
    override fun onCleared() {
        super.onCleared()

        disposables.clear()
    }
}

