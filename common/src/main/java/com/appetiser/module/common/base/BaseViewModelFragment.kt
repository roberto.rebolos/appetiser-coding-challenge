package com.appetiser.module.common.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.appetiser.module.common.di.Injectable
import com.appetiser.module.common.di.ViewModelFactory
import java.lang.reflect.ParameterizedType
import javax.inject.Inject

/**
 *
 * Automatically initializes ViewBinding class and ViewModel class for your activity.
 * VB- for viewbinding
 * VM- instantiating the viewmodel
 */
abstract class BaseViewModelFragment<VB : ViewBinding, VM : BaseViewModel> : BaseFragment<VB>(),
    Injectable {

    lateinit var viewModel: VM

    @Inject
    lateinit var factory: ViewModelFactory<VM>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val viewModelClass = getViewModelClass()
        viewModel = when {
            setActivityAsViewModelProvider() -> {
                ViewModelProvider(requireActivity(), factory)
                    .get(viewModelClass)
            }
            setParentFragmentAsViewModelProvider() -> {
                ViewModelProvider(requireParentFragment(), factory)
                    .get(viewModelClass)
            }
            else -> {
                ViewModelProvider(this, factory)
                    .get(viewModelClass)
            }
        }
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    private fun getViewModelClass(): Class<VM> {
        val type = (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[1]
        return type as Class<VM>
    }

    open fun setParentFragmentAsViewModelProvider(): Boolean = false

    open fun setActivityAsViewModelProvider(): Boolean = false
}
