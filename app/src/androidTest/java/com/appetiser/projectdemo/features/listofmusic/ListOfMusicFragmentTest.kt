package com.appetiser.projectdemo.features.listofmusic

import android.os.Bundle
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.appetiser.module.domain.models.music.Music
import com.appetiser.projectdemo.R
import com.appetiser.projectdemo.features.utils.ManagerFragmentArgs
import com.google.common.truth.Truth.assertThat
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@MediumTest
class ListOfMusicFragmentTest {

    private lateinit var fragmentScenario: FragmentScenario<ListOfMusicFragment>

    @Before
    fun setup() {
        fragmentScenario = launchFragmentInContainer(themeResId = R.style.Theme_Baseplate)
    }

    @Test
    fun test_ListOfMusicFragment_shouldNavigateToMusicSingleFragment() {

        fragmentScenario.onFragment {
            val navController = TestNavHostController(it.requireActivity())
            with(navController) {
                it.requireActivity().runOnUiThread {

                    setGraph(R.navigation.nav_graph)
                    Navigation.setViewNavController(it.requireView(), navController)
                    navigate(R.id.action_listOfMusicFragment_to_musicSingleDescriptionFragment)
                }
                val currentDestination = currentDestination
                assertNotNull(currentDestination)
                assertEquals(currentDestination!!.id, R.id.musicSingleDescriptionFragment)
                Navigation.setViewNavController(it.requireView(), navController)
            }
        }
    }


    @Test
    fun test_ListOfMusicFragment_shouldNavigateToMusicSingleDescriptionFragment() {
        val expectedBundle = Bundle()
        val navController = TestNavHostController(
            ApplicationProvider.getApplicationContext(),
        )
        with(navController) {
            fragmentScenario.onFragment {
                it.requireActivity().runOnUiThread {
                    setGraph(R.navigation.nav_graph)
                }
                Navigation.setViewNavController(it.requireView(), navController)
                navigate(R.id.action_listOfMusicFragment_to_musicSingleDescriptionFragment,
                    expectedBundle)

            }
            val currentDestination = currentDestination!!.id

            assertThat(currentDestination).isEqualTo(R.id.musicSingleDescriptionFragment)
        }
    }


    @Test
    fun test_listOfFragment_shouldNavigateOnlyToMusicSingleDescriptionFragmentAndSameArguments() {
        val args = Bundle()
        args.putParcelable("ARGS_KEY",
            Music(1, "Gogoy", "Test.jpg", 1F, "male", "1", "2", "artistName"))
        val testMusic = Music(1, "Gogoy", "Test.jpg", 1F, "male", "1", "2", "artistName")
        val testNavController = TestNavHostController(ApplicationProvider.getApplicationContext())

            val argsTest = ManagerFragmentArgs(testMusic)
        with(testNavController) {
            val scenario =
                launchFragmentInContainer<ListOfMusicFragment>(themeResId = R.style.Theme_Baseplate,
                    fragmentArgs = argsTest.toBundle())
            scenario.onFragment {
                it.requireActivity().runOnUiThread {
                 setGraph(R.navigation.nav_graph)
                }
                Navigation.setViewNavController(it.requireView(), testNavController)
              navigate(R.id.action_listOfMusicFragment_to_musicSingleDescriptionFragment)
            }
            val currentDestination =currentDestination!!.id
            assertThat(currentDestination).isEqualTo(R.id.musicSingleDescriptionFragment)
            assertThat(ManagerFragmentArgs.fromBundle(argsTest.toBundle())).isEqualTo(argsTest)
        }
    }
}







