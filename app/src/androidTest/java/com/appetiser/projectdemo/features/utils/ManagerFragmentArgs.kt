package com.appetiser.projectdemo.features.utils

import android.os.Bundle
import androidx.navigation.NavArgs
import com.appetiser.module.domain.models.music.Music

data class ManagerFragmentArgs(
    val musicArgs: Music,
) : NavArgs {
    fun toBundle(): Bundle {
        val result = Bundle()
        result.putParcelable("music", this.musicArgs)
        return result
    }

    companion object {
        @JvmStatic
        fun fromBundle(bundle: Bundle): ManagerFragmentArgs {
            bundle.classLoader = ManagerFragmentArgs::class.java.classLoader
            var musicArgs: Music?= if (bundle.containsKey("music")) {
                bundle.getParcelable("music")
            } else {
                null
            }

          return ManagerFragmentArgs(musicArgs!!)
        }
    }
}