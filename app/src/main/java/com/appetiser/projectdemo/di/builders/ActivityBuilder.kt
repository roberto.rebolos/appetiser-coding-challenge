package com.appetiser.projectdemo.di.builders

import com.appetiser.projectdemo.di.scopes.ActivityScope
import com.appetiser.projectdemo.features.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector



@Module
abstract class ActivityBuilder {
    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
}
