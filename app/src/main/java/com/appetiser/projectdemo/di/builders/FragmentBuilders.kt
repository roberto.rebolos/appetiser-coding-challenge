package com.appetiser.projectdemo.di.builders

import com.appetiser.projectdemo.di.scopes.FragmentScope
import com.appetiser.projectdemo.features.description.MusicSingleDescriptionFragment
import com.appetiser.projectdemo.features.listofmusic.ListOfMusicFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector
@Module
abstract class FragmentBuilders {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeListOfFragment(): ListOfMusicFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeSingleDescriptionFragment(): MusicSingleDescriptionFragment
}
