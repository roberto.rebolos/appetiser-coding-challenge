package com.appetiser.projectdemo.di

import android.app.Application
import com.appetiser.module.RoomModule
import com.appetiser.module.data.features.RepositoryModule
import com.appetiser.module.di.BindingMapper
import com.appetiser.module.local.BindingLocalSource
import com.appetiser.module.network.ApiServiceModule
import com.appetiser.module.network.NetworkModule
import com.appetiser.module.network.RemoteSourceModule
import com.appetiser.projectdemo.MusicApplication
import com.appetiser.projectdemo.di.builders.ActivityBuilder
import com.appetiser.projectdemo.di.builders.FragmentBuilders
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ApiServiceModule::class,
        SchedulerModule::class,
        FragmentBuilders::class,
        ActivityBuilder::class,
        RepositoryModule::class,
        RemoteSourceModule::class,
        BindingMapper::class,
        RoomModule::class,
        NetworkModule::class,
        BindingLocalSource::class,
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: MusicApplication)
}
