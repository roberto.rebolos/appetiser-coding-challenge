package com.appetiser.projectdemo.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.appetiser.module.common.utils.displayer.ImageDisplayer
import com.appetiser.module.domain.models.music.Music
import com.appetiser.module.domain.models.music.artWork
import com.appetiser.projectdemo.databinding.CompoundSingleMusicDetailsBinding
import javax.inject.Inject

/**
 * @param imageAdapter - responsible for displaying image using glide
 * @param swipeContainer - responsible for showing refresh when user scroll in the app
 */
class MusicDetailsAdapter @Inject constructor(
    private val imageDisplayer: ImageDisplayer,
    private val swipeContainer: SwipeRefreshLayout,
) :
    RecyclerView.Adapter<MusicDetailsAdapter.ViewHolder>() {
    var onItemClick: ((Music) -> Unit)? = null
     var itemDetailsList: List<Music> = listOf()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): ViewHolder {
        val binding =
            CompoundSingleMusicDetailsBinding.inflate(LayoutInflater.from(parent.context),
                parent,
                false)
        return ViewHolder(binding)
    }

    /**
     * Monitoring users scroll to be diplay in the recycle view
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        swipeContainer.apply {
            when (position) {
                0 -> {
                    isRefreshing = false
                    isEnabled = false
                }
                in 1..48 -> {
                    isRefreshing = true
                    isEnabled = true
                }
                in 49..51 -> {
                    isRefreshing = false
                    isEnabled = false
                }
            }

        }
        holder.bind(itemDetailsList[position])
    }

    // geting the size of the list
    override fun getItemCount(): Int {
        return itemDetailsList.size
    }


    /**
     * responsible for updating the array adapter
     * @param listOfMusic - list of domain model
     */
    fun updateItunesData(listOfMusic: List<Music>) {

        this.itemDetailsList = listOfMusic
        notifyDataSetChanged()


    }

    /**
     * Responsible for binding our views
     */
    inner class ViewHolder(
        private val itemsDetailsBinding: CompoundSingleMusicDetailsBinding,
    ) :
        RecyclerView.ViewHolder(itemsDetailsBinding.root) {
        /**
         * binding the view to the data coming domain model
         * @param musicDomain - getting the data coming from
         * our domain model
         */
        fun bind(musicDomain: Music) {
            Log.d("TEST", "Music data $musicDomain")
            /**
             * Attaching the data coming from remote and saving it via room persistence
             * to the views
             */
            itemsDetailsBinding.apply {
                trackNameText.text = musicDomain.name
                trackGenreText.text = musicDomain.genre

                trackPriceText.text = musicDomain.price.toString()
                musicDomain.artWork.let { modifiedArtwork ->
                    imageDisplayer.displayImage(
                        albumImage = itemsDetailsBinding.artwork,
                        modifiedArtwork
                    )
                }
                // handling click events of the views
                itemsDetailsBinding.root.setOnClickListener {
                    Log.d("TEST", "NA CLICK BAY")
                    onItemClick?.invoke(musicDomain)
                }
            }


        }

    }
}