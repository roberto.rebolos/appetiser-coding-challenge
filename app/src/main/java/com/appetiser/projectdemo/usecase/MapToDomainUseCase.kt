package com.appetiser.projectdemo.usecase

import com.appetiser.module.data.features.music.MusicRepository
import com.appetiser.module.domain.models.music.Music
import com.appetiser.module.local.features.music.models.MusicLocalEntity
import javax.inject.Inject

class MapToDomainUseCase @Inject constructor(private val itunesRepository: MusicRepository) {
    /**
     * @param listOfLocalEntity - list of local entity data stored in db
     * @return List<Music> - list of music domain data
     */
    fun toDomain(listOfLocalEntity: List<MusicLocalEntity>): List<Music> {
        return itunesRepository.mapLocalToDomain(listOfLocalEntity)
    }
}