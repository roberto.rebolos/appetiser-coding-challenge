package com.appetiser.projectdemo.usecase


import com.appetiser.module.data.features.music.MusicRepository
import com.appetiser.module.local.features.music.models.MusicLocalEntity
import io.reactivex.Single
import javax.inject.Inject


class GetItunesItemDataUseCase @Inject constructor(private val trackRepository: MusicRepository) {
    // getting all music data stored in db
    fun getAllItunesMusic(): Single<List<MusicLocalEntity>> =
        trackRepository.getAllMusicItemsStoredInDb()


}