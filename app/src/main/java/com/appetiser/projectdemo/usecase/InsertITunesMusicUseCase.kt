package com.appetiser.projectdemo.usecase

import android.util.Log
import com.appetiser.module.data.features.music.MusicRepository
import com.appetiser.module.network.features.music.model.MusicNetworkEntity
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class InsertITunesMusicUseCase @Inject constructor(private val itunesRepository: MusicRepository) {
    /**
     * @param networkList - list of dto network entity
     * @return Completable- Either for completion inserting the data or error
     */
    fun insertItunesMusic(networkList: List<MusicNetworkEntity>): Completable {
        val listOfMusicLocal = itunesRepository.mapNetworkEntityToLocalEntity(networkList)
        Log.d("TEST", "List of music Size  ${listOfMusicLocal.size}")
        return itunesRepository.insertMusicItem(listOfMusicLocal)

    }
}