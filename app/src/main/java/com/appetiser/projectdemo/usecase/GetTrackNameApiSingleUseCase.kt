package com.appetiser.projectdemo.usecase


import com.appetiser.module.data.features.music.MusicRepository
import com.appetiser.module.domain.core.Empty
import com.appetiser.module.network.base.response.BaseResponse
import com.appetiser.module.network.features.music.model.MusicNetworkEntity
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetTrackNameApiSingleUseCase @Inject constructor(
    private val itunesRepository: MusicRepository,
    ) : SingleUseCase<Empty, BaseResponse<List<MusicNetworkEntity>>>() {


    override fun executeUseCase(requestValues: Empty): Single<BaseResponse<List<MusicNetworkEntity>>> {
        return itunesRepository.getSongList("star", "au", "movie")

    }
}




