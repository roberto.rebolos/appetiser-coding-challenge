package com.appetiser.projectdemo

import android.app.Application
import com.appetiser.projectdemo.di.AppInjector
import com.facebook.stetho.Stetho
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.reactivex.plugins.RxJavaPlugins
import timber.log.Timber
import javax.inject.Inject

class MusicApplication : Application(), HasAndroidInjector {

    /* This is the new update from dagger you can view this
as your reference https://github.com/google/dagger/commit/3bd8f707cb28fd0c5f3abb4f87658566f8b52c10
for reducing the boiler plate
    */
    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    /** Returns an [AndroidInjector].  */
    override fun androidInjector(): DispatchingAndroidInjector<Any> {
        return androidInjector
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
            Timber.plant(Timber.DebugTree())
        }

        // Rx Error Handler
        RxJavaPlugins.setErrorHandler { e -> Timber.e(e) }

        AppInjector.init(this)
    }
}
