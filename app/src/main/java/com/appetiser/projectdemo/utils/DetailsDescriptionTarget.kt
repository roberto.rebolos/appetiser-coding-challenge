package com.appetiser.projectdemo.utils

// Specifying the target on which to display the music domain
enum class DetailsDescriptionTarget {

    TRACKNAME, GENRE, ARTIST, PRICE, MATURITY_RATING, LONG_DESCRIPTION


}