package com.appetiser.projectdemo.utils

/**
 * @param detailsDescriptionTarget- address of the data to display in view
 * @param data- field data for domain to be use to display in viewmodel
 */
class DetailsStorage(val detailsDescriptionTarget: DetailsDescriptionTarget, val data: String)