package com.appetiser.projectdemo.features.description

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.navigation.fragment.navArgs
import com.appetiser.module.common.base.BaseViewModelFragment
import com.appetiser.projectdemo.databinding.FragmentMusicDetailsBinding
import com.appetiser.projectdemo.di.scopes.FragmentScope
import com.appetiser.projectdemo.utils.DetailsDescriptionTarget
import com.appetiser.projectdemo.utils.DetailsStorage
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

@FragmentScope
class MusicSingleDescriptionFragment :
    BaseViewModelFragment<FragmentMusicDetailsBinding, ItunesSingleDescriptionViewModel>() {

    private val args: MusicSingleDescriptionFragmentArgs by navArgs()

    override fun getViewBinding(): FragmentMusicDetailsBinding =
        FragmentMusicDetailsBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d("TEST", "Track name ${binding.trackNameText.text.toString()}")
        super.onViewCreated(view, savedInstanceState)
        subscribeToObservers()
    }

    override fun onResume() {
        super.onResume()

        viewModel.bindMusicData(args.music)
        subscribeToObservers()
    }

    // subscribing the state
    private fun subscribeToObservers() {
        viewModel.stateSingleMusic
            .observeOn(scheduler.ui())
            .subscribeBy(onNext = ::getDetailState,
                onError = {
                    Log.d("ERROR", "Error handling state $it")
                },
                onComplete = {
                    Log.d("ERROR", "Complete na ")
                }
            ).addTo(disposables)

        viewModel.albumArtworkState
            .observeOn(scheduler.ui())
            .subscribeBy(onNext = {
                imageDisplayer.displayImage(binding.artwork, it)
            },
                onError = {
                    Log.d("ERROR", " ERROR FETCHING $it")
                }).addTo(disposables)
    }

    // getting the emitted state values
    private fun getDetailState(detailStateSingleMusic: SingleMusicItemState) {
        binding.apply {
            when (detailStateSingleMusic) {
                is SingleMusicItemState.SingleMusicItemStorage -> {
                    displayMusicInformation(detailStateSingleMusic.dataState)

                }
            }

        }
    }

    // diplaying the music info
    private fun displayMusicInformation(musicDetailsList: MutableList<DetailsStorage>) {
        binding.apply {
            musicDetailsList.forEach {
                Log.d("TEST", "Size of details ${musicDetailsList.size}")
                val musicDetailsTarget = when (it.detailsDescriptionTarget) {
                    DetailsDescriptionTarget.TRACKNAME -> trackNameText
                    DetailsDescriptionTarget.GENRE -> genreText
                    DetailsDescriptionTarget.ARTIST -> artistText
                    DetailsDescriptionTarget.PRICE -> priceText
                    DetailsDescriptionTarget.MATURITY_RATING -> maturityText
                    DetailsDescriptionTarget.LONG_DESCRIPTION -> longDescriptionText
                }
                musicDetailsTarget.text = it.data
            }
        }
    }

}