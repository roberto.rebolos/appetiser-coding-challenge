package com.appetiser.projectdemo.features.description

import com.appetiser.projectdemo.utils.DetailsStorage

sealed class SingleMusicItemState {
    object IDLE: SingleMusicItemState()
    data class SingleMusicItemStorage(val dataState: MutableList<DetailsStorage>) : SingleMusicItemState()
}
