package com.appetiser.projectdemo.features.listofmusic


import android.util.Log
import com.appetiser.module.common.base.BaseViewModel
import com.appetiser.module.domain.core.Empty
import com.appetiser.projectdemo.usecase.GetItunesItemDataUseCase
import com.appetiser.projectdemo.usecase.GetTrackNameApiSingleUseCase
import com.appetiser.projectdemo.usecase.InsertITunesMusicUseCase
import com.appetiser.projectdemo.usecase.MapToDomainUseCase
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject


class ListOfMusicViewModel @Inject constructor(
    private val getTrackNameApiUseCase: GetTrackNameApiSingleUseCase,
    private val getItunesItemDataUseCase: GetItunesItemDataUseCase,
    private val insertITunesMusicUseCase: InsertITunesMusicUseCase,
    private val mapToDomainUseCase: MapToDomainUseCase,
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<ListOfDetailsState>()
    }
    val state: Observable<ListOfDetailsState> = _state


    /**
     * get the music details coming from itunes remote webservices
     */
    var empty = Empty()
    fun getMusicFromItunesApi() {

        getTrackNameApiUseCase.run(empty)
            .subscribeOn(schedulerProvider.io())
            .flatMapCompletable {
                Log.d("Complete", "on Complete")
                insertITunesMusicUseCase.insertItunesMusic(it.results)
            }
            .observeOn(schedulerProvider.ui())
            .subscribeBy(
                onComplete = {
                    Log.d("Complete", "Complete 1")
                    getItunesItemDataUseCase.getAllItunesMusic()
                        .subscribeOn(schedulerProvider.io())
                        .observeOn(schedulerProvider.ui())
                        .subscribeBy(onSuccess = {
                            Log.d("Complete", "on Success na")
                            val domainData = mapToDomainUseCase.toDomain(it)
                            _state.onNext(ListOfDetailsState.Success(domainData))
                        },
                            onError = {
                                _state.onError(it.fillInStackTrace())
                            }).addTo(disposables)

                },
                onError = {
                    Log.d("TEST", "Error man ${it.localizedMessage}")
                    _state.onError(it.fillInStackTrace())
                }).addTo(disposables)

    }

    /**
     * Responsible for getting all the music data stored in db
     */
    fun getAllMusicInDb() {
        getItunesItemDataUseCase.getAllItunesMusic()
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribeBy {
                val domainData = mapToDomainUseCase.toDomain(it)
                _state.onNext(ListOfDetailsState.Success(domainData))
            }.addTo(disposables)
    }
}



