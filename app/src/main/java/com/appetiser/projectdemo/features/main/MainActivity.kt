package com.appetiser.projectdemo.features.main

import com.appetiser.module.common.base.BaseActivity
import com.appetiser.projectdemo.databinding.ActivityMainBinding

class MainActivity : BaseActivity<ActivityMainBinding>() {
    override fun getViewBinding(): ActivityMainBinding = ActivityMainBinding.inflate(layoutInflater)

}
