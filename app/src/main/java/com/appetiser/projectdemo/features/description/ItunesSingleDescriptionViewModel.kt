package com.appetiser.projectdemo.features.description


import com.appetiser.module.common.base.BaseViewModel
import com.appetiser.module.domain.models.music.Music
import com.appetiser.module.domain.models.music.artWork
import com.appetiser.projectdemo.utils.DetailsDescriptionTarget
import com.appetiser.projectdemo.utils.DetailsStorage
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject


class ItunesSingleDescriptionViewModel @Inject constructor(
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<SingleMusicItemState>()
    }
    val stateSingleMusic: Observable<SingleMusicItemState> = _state

    private val _albumArtworkState = PublishSubject.create<String>()
    val albumArtworkState: Observable<String> = _albumArtworkState

    /**
     * Responsible for binding the data when  in onResume state
     */
    fun bindMusicData(music: Music) {
        val musicDataList = mutableListOf<DetailsStorage>()
        music.apply {
            musicDataList.add(DetailsStorage(DetailsDescriptionTarget.TRACKNAME, name))
            musicDataList.add(DetailsStorage(DetailsDescriptionTarget.GENRE, genre))
            musicDataList.add(DetailsStorage(DetailsDescriptionTarget.ARTIST, artistName))
            musicDataList.add(DetailsStorage(DetailsDescriptionTarget.PRICE, price.toString()))
            musicDataList.add(DetailsStorage(DetailsDescriptionTarget.MATURITY_RATING,
                maturityText))
            musicDataList.add(DetailsStorage(DetailsDescriptionTarget.LONG_DESCRIPTION,
                description))
            getMusicAlbumArtwork(musicArtwork = artWork)
        }
        _state.onNext(SingleMusicItemState.SingleMusicItemStorage(musicDataList))
    }

    /**
     * Displaying the music album artwork
     */
    private fun getMusicAlbumArtwork(
        musicArtwork: String,

        ) {
        Single
            .just(musicArtwork)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribeBy(
                onSuccess = {
                    _albumArtworkState.onNext(it)
                },
                onError = {
                    _albumArtworkState.onError(it)
                }
            ).addTo(disposables)
    }
}
