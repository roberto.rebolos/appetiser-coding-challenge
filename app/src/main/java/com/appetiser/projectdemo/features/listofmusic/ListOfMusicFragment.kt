package com.appetiser.projectdemo.features.listofmusic

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.appetiser.module.common.base.BaseViewModelFragment
import com.appetiser.module.common.extensions.safeNavigate
import com.appetiser.module.common.extensions.toast
import com.appetiser.projectdemo.adapter.MusicDetailsAdapter
import com.appetiser.projectdemo.databinding.FragmentDetailsContainerRecycleviewBinding
import com.appetiser.projectdemo.di.scopes.FragmentScope
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

@FragmentScope
class ListOfMusicFragment :
    BaseViewModelFragment<FragmentDetailsContainerRecycleviewBinding, ListOfMusicViewModel>() {
    override fun getViewBinding(): FragmentDetailsContainerRecycleviewBinding =
        FragmentDetailsContainerRecycleviewBinding.inflate(layoutInflater)

    lateinit var musicAdapter: MusicDetailsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // get the music from itunes
        viewModel.getMusicFromItunesApi()
        setUpVmObservers()
        setUpArrayAdapter()

        musicAdapter.onItemClick = {
            findNavController().safeNavigate(ListOfMusicFragmentDirections.actionListOfMusicFragmentToMusicSingleDescriptionFragment(
                it))
        }
    }

    /**
     * Subscribing on the data emitted in the viewmodel
     */
    private fun setUpVmObservers() {
        viewModel.state.observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    it.message?.let { serverMessage -> requireContext().toast(serverMessage) }
                },
                onComplete = {
                }
            ).addTo(compositeDisposable = disposables)
    }

    // handling the state to display in adapter
    private fun handleState(state: ListOfDetailsState) {
        when (state) {
            is ListOfDetailsState.Success -> {
                musicAdapter.updateItunesData(state.successState)
            }
            ListOfDetailsState.IDLE -> {
                // No Operation //
            }
        }
    }

    // setting up the array adapter
    private fun setUpArrayAdapter() {
        musicAdapter =
            MusicDetailsAdapter(
                imageDisplayer = imageDisplayer,
                binding.swipeContainer
            )
        binding.apply {
            recycleViewItunesList.apply {
                adapter = musicAdapter
                layoutManager =
                    GridLayoutManager(requireContext(), 2)
            }
        }
    }
}