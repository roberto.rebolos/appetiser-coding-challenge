package com.appetiser.projectdemo.features.listofmusic

import com.appetiser.module.domain.models.music.Music


sealed class ListOfDetailsState {
    object IDLE : ListOfDetailsState()
    data class Success(val successState: List<Music>) : ListOfDetailsState()
}