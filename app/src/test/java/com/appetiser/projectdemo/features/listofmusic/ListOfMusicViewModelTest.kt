package com.appetiser.projectdemo.features.listofmusic

import com.appetiser.module.domain.core.Empty
import com.appetiser.module.domain.models.music.Music
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.domain.utils.whenever
import com.appetiser.module.local.features.music.models.MusicLocalEntity
import com.appetiser.module.network.base.api.AppleApiServices
import com.appetiser.module.network.features.music.model.MusicNetworkEntity
import com.appetiser.projectdemo.Stubs
import com.appetiser.projectdemo.Stubs.LIST_OF_MUSIC_DOMAIN
import com.appetiser.projectdemo.core.BaseViewModelTest
import com.appetiser.projectdemo.usecase.GetItunesItemDataUseCase
import com.appetiser.projectdemo.usecase.GetTrackNameApiSingleUseCase
import com.appetiser.projectdemo.usecase.InsertITunesMusicUseCase
import com.appetiser.projectdemo.usecase.MapToDomainUseCase
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito
import org.mockito.Mockito.mock


class ListOfMusicViewModelTest : BaseViewModelTest() {


    private lateinit var listOfMusicViewModel: ListOfMusicViewModel
    private val getTrackNameApiSingleUseCase = mock(GetTrackNameApiSingleUseCase::class.java)
    private val getItunesItemDataUseCase = mock(GetItunesItemDataUseCase::class.java)
    private val insertITunesMusicUseCase: InsertITunesMusicUseCase =
        mock(InsertITunesMusicUseCase::class.java)
    private val mapToDomainUseCase = mock(MapToDomainUseCase::class.java)
    private val appleApiServices = mock(AppleApiServices::class.java)

    private val observer: TestObserver<ListOfDetailsState> = mock()
    private val empty = mock<Empty>()

    @Before
    fun setup() {
        listOfMusicViewModel = ListOfMusicViewModel(
            getItunesItemDataUseCase = getItunesItemDataUseCase,
            getTrackNameApiUseCase = getTrackNameApiSingleUseCase,
            insertITunesMusicUseCase = insertITunesMusicUseCase,
            mapToDomainUseCase = mapToDomainUseCase,
        )
        listOfMusicViewModel.schedulerProvider = schedulers
        listOfMusicViewModel.empty = empty
        listOfMusicViewModel.state.subscribe(observer)
    }

    @Test
    fun getMusicFromApi_WhenTheAllQueryIsNotEmpty() {

        val expected = Stubs.LIST_OF_BASERESPONSE

        val expectedObserver = ListOfDetailsState.Success(listOf<Music>())
        val listOfNetwork = listOf<MusicNetworkEntity>()
        val listOfDomainLocal = listOf<MusicLocalEntity>()

        whenever(getTrackNameApiSingleUseCase.run(empty)).thenReturn(Single.just(expected))

        whenever(insertITunesMusicUseCase.insertItunesMusic(listOfNetwork)).thenReturn(Completable.complete())

        whenever(getItunesItemDataUseCase.getAllItunesMusic()).thenReturn(Single.just(Stubs.LIST_OF_MUSIC_LOCAL))
        listOfMusicViewModel.getAllMusicInDb()
        testScheduler.triggerActions()
        whenever(mapToDomainUseCase.toDomain(listOfDomainLocal)).thenReturn(LIST_OF_MUSIC_DOMAIN)

        listOfMusicViewModel.getMusicFromItunesApi()




        ArgumentCaptor.forClass(ListOfDetailsState::class.java).run {
            Mockito.verify(observer, Mockito.times(1)).onNext(capture())
            assertEquals(expectedObserver, value)
        }
    }
}