package com.appetiser.projectdemo

import com.appetiser.module.domain.models.music.Music
import com.appetiser.module.local.features.music.models.MusicLocalEntity
import com.appetiser.module.network.base.response.BaseResponse
import com.appetiser.module.network.features.music.model.MusicNetworkEntity

object Stubs {
    val LIST_OF_BASERESPONSE = BaseResponse(0, listOf<MusicNetworkEntity>(
        MusicNetworkEntity(
            1,
            "Gogoy",
            "test_image",
            price = 0f,
            genre = "genre",
            description = "description",
            artistName = "Star wars",
            maturity = "maturity_test")
    ))

    val LIST_OF_MUSIC_LOCAL = listOf<MusicLocalEntity>(MusicLocalEntity(
        id = 1L,
        name = "Jackson",
        "http:// a1.itunes.apple.com/jp/r10/Music/ y2005/m06/d03/h05/ s05.oazjtxkw.100×100-75.jpg",
        artistName = "Bradley Cooper",
        description = "Seasoned musician Jackson Maine (Bradley Cooper) discovers—and falls in love with—struggling artist Ally (Lady Gaga). She has just about given up on her dream to make it big as a singer… until Jack coaxes her into the spotlight. But even as Ally’s career takes off, the personal side of their relationship is breaking down, as Jack fights an ongoing battle with his own internal demons.",
        genre = "Action & Genre",
        maturityText = "Gogoy",
        price = 18.99F))
    val LIST_OF_MUSIC_DOMAIN = listOf<Music>(Music(
        id = 1L,
        name = "Jackson",
        "http:// a1.itunes.apple.com/jp/r10/Music/ y2005/m06/d03/h05/ s05.oazjtxkw.100×100-75.jpg",
        artistName = "Bradley Cooper",
        description = "Seasoned musician Jackson Maine (Bradley Cooper) discovers—and falls in love with—struggling artist Ally (Lady Gaga). She has just about given up on her dream to make it big as a singer… until Jack coaxes her into the spotlight. But even as Ally’s career takes off, the personal side of their relationship is breaking down, as Jack fights an ongoing battle with his own internal demons.",
        genre = "Action & Genre",
        maturityText = "Gogoy",
        price = 18.99F))
}
