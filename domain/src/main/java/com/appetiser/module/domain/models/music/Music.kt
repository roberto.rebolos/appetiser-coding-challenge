package com.appetiser.module.domain.models.music

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
// data for UI to display
data class Music(
    val id: Long = 0,
    var name: String,
    val imageURL: String,
    val price: Float,
    val genre: String,
    val description: String = "",
    val maturityText: String,
    val artistName: String,
) : Parcelable

/**
 * Manipulate URL to get high quality image
 */
val Music.artWork: String get() = imageURL.replace("100x100", "600x600")