package com.appetiser.module

import android.app.Application
import androidx.room.Room

import com.appetiser.module.local.db.ItunesItemDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {
    @Provides
    @Singleton
    fun providesItemsItunesDatabase(app: Application) =
        Room.databaseBuilder(
            app.applicationContext,
            ItunesItemDatabase::class.java,
            DATABASE_NAME
        )
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun providesItunesItemDao(itunesItemDatabase: ItunesItemDatabase) =
        itunesItemDatabase.itunesDao()
}