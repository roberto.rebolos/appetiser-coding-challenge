package com.appetiser.module.local.features.music

import com.appetiser.module.local.features.music.dao.ItunesItemDao
import com.appetiser.module.local.features.music.models.MusicLocalEntity
import io.reactivex.Completable
import io.reactivex.Single


import javax.inject.Inject

class MusicLocalImpl @Inject constructor(private val itunesItemDao: ItunesItemDao) :
    MusicLocalSource {
    override fun insert(musicLocalEntity: List<MusicLocalEntity>): Completable {
       return itunesItemDao.insert(musicLocalEntity = musicLocalEntity)
    }

    override fun getAllMusicItems(): Single<List<MusicLocalEntity>> {
        return itunesItemDao.getAllMusicItems()
    }
}