package com.appetiser.module.local

import com.appetiser.module.local.features.music.MusicLocalImpl
import com.appetiser.module.local.features.music.MusicLocalSource
import dagger.Binds
import dagger.Module
import javax.inject.Singleton
@Module
abstract class BindingLocalSource {
    @Binds
    @Singleton
    abstract fun bindsLocalSource(musicLocalImpl: MusicLocalImpl): MusicLocalSource
}