package com.appetiser.module.local.features.music

import com.appetiser.module.local.features.music.models.MusicLocalEntity
import io.reactivex.Completable
import io.reactivex.Single


interface MusicLocalSource {
    fun insert(musicLocalEntity: List<MusicLocalEntity>): Completable

    fun getAllMusicItems(): Single<List<MusicLocalEntity>>
}