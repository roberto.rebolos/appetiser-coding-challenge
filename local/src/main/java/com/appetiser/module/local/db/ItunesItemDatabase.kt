package com.appetiser.module.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.appetiser.module.local.features.music.dao.ItunesItemDao
import com.appetiser.module.local.features.music.models.MusicLocalEntity

@Database(
    entities = [MusicLocalEntity::class],
    version = 1)
abstract class ItunesItemDatabase : RoomDatabase() {

    abstract fun itunesDao(): ItunesItemDao

}