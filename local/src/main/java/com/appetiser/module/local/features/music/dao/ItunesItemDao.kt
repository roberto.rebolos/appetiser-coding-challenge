package com.appetiser.module.local.features.music.dao

import androidx.room.*
import com.appetiser.module.ITUNES_TABLE_NAME
import com.appetiser.module.local.features.music.models.MusicLocalEntity
import io.reactivex.Completable
import io.reactivex.Single


@Dao
interface ItunesItemDao {
    @Query("SELECT * FROM $ITUNES_TABLE_NAME ORDER BY id DESC")
    fun getAllMusicItems(): Single<List<MusicLocalEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(musicLocalEntity: List<MusicLocalEntity>): Completable

}