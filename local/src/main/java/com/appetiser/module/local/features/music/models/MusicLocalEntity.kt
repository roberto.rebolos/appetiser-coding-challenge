package com.appetiser.module.local.features.music.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.appetiser.module.ITUNES_TABLE_NAME

// for local entity db data class
@Entity(tableName = ITUNES_TABLE_NAME)
data class MusicLocalEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    @ColumnInfo(name = "name")
    var name: String,
    @ColumnInfo(name = "trackUrl")
    val imageURL: String,
    @ColumnInfo(name = "price")
    val price: Float,
    @ColumnInfo(name = "genre")
    val genre: String,
    @ColumnInfo(name = "description")
    val description: String,
    @ColumnInfo(name = "maturity")
    val maturityText: String?,
    @ColumnInfo(name = "artistName")
    val artistName: String,
)