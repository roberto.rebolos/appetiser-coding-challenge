package com.appetiser.module.local.features.music


import com.appetiser.module.domain.utils.whenever
import com.appetiser.module.local.features.Stubs
import com.appetiser.module.local.features.music.dao.ItunesItemDao
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock


class MusicLocalImplTest {

    private val itunesDao: ItunesItemDao = mock(ItunesItemDao::class.java)
    private lateinit var subject: MusicLocalSource

    @Before
    fun setup() {
        subject = MusicLocalImpl(itunesDao)
    }


    @Test
    fun insertMusic_ShouldInsertAMusicItem() {
        val expected = Stubs.LIST_OF_MUSIC
       whenever(itunesDao.insert(expected)).thenReturn(Completable.complete())
        whenever(itunesDao.getAllMusicItems()).thenReturn(Single.just(expected))
        subject.getAllMusicItems().test().assertComplete().assertValue {
            it == expected
        }
    }

}