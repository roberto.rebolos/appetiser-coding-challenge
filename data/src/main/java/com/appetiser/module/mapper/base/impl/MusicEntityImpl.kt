package com.appetiser.module.mapper.base.impl

import android.util.Log
import com.appetiser.module.local.features.music.models.MusicLocalEntity
import com.appetiser.module.mapper.base.EntityMapper
import com.appetiser.module.network.features.music.model.MusicNetworkEntity
import javax.inject.Inject

class MusicEntityImpl @Inject constructor() :
    EntityMapper<MusicNetworkEntity, MusicLocalEntity> {
    // mapping the data from network to local db entity
    override fun mapFromEntity(dataResponse: MusicNetworkEntity): MusicLocalEntity {
        return MusicLocalEntity(
            id = dataResponse.id,
            name = dataResponse.name,
            price = dataResponse.price,
            genre = dataResponse.genre,
            description = dataResponse.description ?: "",
            maturityText = dataResponse.maturity,
            imageURL = dataResponse.imageURL,
            artistName = dataResponse.artistName
        )
    }

    // mapping the data from local to network entity
    override fun mapToEntity(data: MusicLocalEntity): MusicNetworkEntity {
        return MusicNetworkEntity(
            id = data.id,
            description = data.description,
            genre = data.genre,
            imageURL = data.imageURL,
            maturity = data.maturityText ?: "",
            name = data.name,
            price = data.price,
            artistName = data.artistName
        )
    }
    // transforming the list into data class
    override fun map(response: List<MusicNetworkEntity>): List<MusicLocalEntity> {
        return response.map {
            Log.d("TEST", "Data map ${it.toString()}")
            mapFromEntity(dataResponse = it)
        }
    }


}