package com.appetiser.module.data.features

import com.appetiser.module.data.features.music.MusicRepository
import com.appetiser.module.data.features.music.MusicRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {


    @Binds
    @Singleton
    abstract fun bindsRepositoryModule(repositoryImpl: MusicRepositoryImpl): MusicRepository
}
