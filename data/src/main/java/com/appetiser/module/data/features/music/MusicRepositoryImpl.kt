package com.appetiser.module.data.features.music


import com.appetiser.module.domain.models.music.Music
import com.appetiser.module.local.features.music.MusicLocalSource
import com.appetiser.module.local.features.music.models.MusicLocalEntity
import com.appetiser.module.mapper.base.EntityMapper
import com.appetiser.module.network.base.response.BaseResponse
import com.appetiser.module.network.features.music.MusicItunesRemoteSource
import com.appetiser.module.network.features.music.model.MusicNetworkEntity
import io.reactivex.Completable

import io.reactivex.Single
import javax.inject.Inject

class MusicRepositoryImpl @Inject constructor(
    private val localSource: MusicLocalSource,
    private val itunesRemoteSource: MusicItunesRemoteSource,
    private val mapper: EntityMapper<MusicNetworkEntity, MusicLocalEntity>,
    private val mapperToDomain: EntityMapper<MusicLocalEntity, Music>,
) : MusicRepository {
    override fun getAllMusicItemsStoredInDb(): Single<List<MusicLocalEntity>> {
        return localSource.getAllMusicItems()
    }

    override fun getSongList(
        term: String,
        country: String,
        media: String,
    ): Single<BaseResponse<List<MusicNetworkEntity>>> {
        return itunesRemoteSource.getSongList(
            term = term,
            country = country,
            media = media
        )
    }


    override fun insertMusicItem(musicEntity: List<MusicLocalEntity>): Completable {
        return localSource.insert(musicEntity)
    }


    override fun mapNetworkEntityToLocalEntity(list: List<MusicNetworkEntity>): List<MusicLocalEntity> {
        return mapper.map(response = list)
    }

    override fun mapLocalToDomain(list: List<MusicLocalEntity>): List<Music> {
        return mapperToDomain.map(list)
    }


}