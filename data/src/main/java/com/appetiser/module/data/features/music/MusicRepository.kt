package com.appetiser.module.data.features.music
import com.appetiser.module.domain.models.music.Music
import com.appetiser.module.local.features.music.models.MusicLocalEntity
import com.appetiser.module.network.base.response.BaseResponse
import com.appetiser.module.network.features.music.model.MusicNetworkEntity
import io.reactivex.Completable
import io.reactivex.Single

interface MusicRepository {

    fun getAllMusicItemsStoredInDb(): Single<List<MusicLocalEntity>>

    fun getSongList(
        term: String,
        country: String,
        media: String
    ): Single<BaseResponse<List<MusicNetworkEntity>>>

    fun insertMusicItem(musicEntity: List<MusicLocalEntity>):Completable

    fun mapNetworkEntityToLocalEntity(list: List<MusicNetworkEntity>): List<MusicLocalEntity>

    fun mapLocalToDomain(list: List<MusicLocalEntity>): List<Music>
}