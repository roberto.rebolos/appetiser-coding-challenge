package com.appetiser.module.di

import com.appetiser.module.domain.models.music.Music
import com.appetiser.module.local.features.music.models.MusicLocalEntity
import com.appetiser.module.mapper.base.EntityMapper
import com.appetiser.module.mapper.base.impl.MusicCacheImpl
import com.appetiser.module.mapper.base.impl.MusicEntityImpl
import com.appetiser.module.network.features.music.model.MusicNetworkEntity
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class BindingMapper {
    @Binds
    @Singleton
    abstract fun bindsMusicCacheImpl(musicCacheImpl: MusicCacheImpl): EntityMapper<MusicLocalEntity, Music>

    @Binds
    @Singleton
    abstract fun bindsMusicEntity(musicEntity: MusicEntityImpl): EntityMapper<MusicNetworkEntity, MusicLocalEntity>
}