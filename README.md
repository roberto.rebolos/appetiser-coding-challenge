# AppetiserCodingChallenge

Create a master-detail application that contains at least one dependency. This application should display a list of items obtained from a iTunes Search API and show a detailed view of each item. The URL you must obtain your data from is as follows:

https://itunes.apple.com/search?term=star&country=au&media=movie&;all

(iTunes Web Service Documentation: https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/#searching)

Note: In this project I've simple unit testing using junit framework 

Getting Started Clone this repository by using the comamnd lines below

git clone https://gitlab.com/roberto.rebolos/appetiser-coding-challenge
find the directory folder
import it via Android Studio

Technologies Used

RxKotlin & Rx Android - to handle multi threading especially on calling network request and database operation to make the app have a good user experience
because multi threading helps the app to run smooth and efficient.

Dagger Android - to easily handle dependency injection to the consumer classes without using manual injection to make the code
testable and maintainable along in the long run

Retrofit - to easily connect with webservices

Glide- to display images and also to keep taking care of caching and keeping a low memory impact when doing image manipulations.

Swipe Refresher- in order to provide a good user experience when the images are loaded in the recycle view

AndroidX Room - SQLite ORM for Android, for a much lesser boilerplate code and to prevent runtime error on our queries statement which I believe that it will
be really hard to debug.

Project Architecture

This app uses the MVVM architecture as recommended by Google with repository pattern and usecase to handle the domain layer

1) App module - This module contains app UI related classes and resources
  
  Classes that inside of this module are the following below
     - Activities

     - Fragments

     - View Model

     - UseCase - for domain layer stuff and handling business logic

     - Resources - constains (e.g. layout files, navigation,styles and etc)


2) Data module - This module is responsible for providing data either coming from local or network

  Classes inside of this module 

     - MusicRepository interface

     - MusicRepositoryImpl 

     - Mapper Interface

     - MusicEntityImpl

     - MusicCacheEntityImpl

3)Network Module - This module is responsible for handling & providing data coming from API which All API must be called here.

  Classes inside of this module
     - MusicItunesRemoteSource interface

     - MusicItunesRemouteSourceImpl

     - Responses data classes for API calls

     - MusicNetworkDtoEntity - this is an object that represents objects for a JSON API Response
  
4)Domain Module - This is where the app,data,local and network communicates to turn the MusicLocalEntity into Music Domain

   Clases inside of this module

     - Music responsible for data source on how we should update our UI
 
5 Common Module - This module is responsible for giving common fields and methods of the app in order to make the code testable
and maintainable because it follows SOLID Principle pattern

  Packages inside of this module 

     - base this is where we define all the base classes such as Activity, Fragment, BaseViewModel which these classes constains methods and attributes 
      that are the same with most of the child classes in the app for example Activities,ViewModels and Fragments 

     - di constains depedencies that are common with other modules

     - extensions - constains an extension functions
     
     - utils - contains schedulers for rx java stuff

 
 What are the benefits of this architecture? It has the following benefits below are:

 -separation of concerns- each layer has it's only specific job that it does not interfere with the other layers.

 -Repository Pattern- to abstract multiple data source, whether it maybe local cache or from a network fetch by using mapper interface
  to map the dto to cache model and also to help map the cache data to domain model
  
  Usecases-for handling business logic or domain layer
  
  To know more about this architecture you may visit this link below
  -https://proandroiddev.com/why-you-need-use-cases-interactors-142e8a6fe576
  - https://medium.com/swlh/repository-pattern-in-android-c31d0268118c
  - https://www.tutorialspoint.com/mvvm/mvvm_advantages.html
  
 
Authors and Acknowledgment This is for Appetiser Apps submission purposes only.

Roberto A Rebolos Jr

