package com.appetiser.module.network.features.music
import com.appetiser.module.network.base.api.AppleApiServices
import com.appetiser.module.network.features.Stubs
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

class MusicItunesRemoteSourceImplTest {

    private val appleApiServices = mock(AppleApiServices::class.java)

    private lateinit var subject: MusicItunesRemoteSource

    @Before
    fun setup() {
        subject = MusicItunesRemoteSourceImpl(appleApiServices)
    }


    @Test
    fun getSongList_ShouldReturnABaseResponse() {
        val term = "star"
        val country = "au"
        val media = "movie"
        val response = Stubs.LIST_OF_BASERESPONSE
        Mockito.`when`(appleApiServices.getSongList(term,
            country,
            media)).thenReturn(Single.just(response))

        subject.getSongList(term, country, media).test().assertComplete().assertValue {
            it == response
        }
        Mockito.verify(appleApiServices, Mockito.times(1)).getSongList(term, country, media)
    }

}
