package com.appetiser.module.network.features.music


import com.appetiser.module.network.base.api.AppleApiServices
import com.appetiser.module.network.base.response.BaseResponse
import com.appetiser.module.network.features.music.model.MusicNetworkEntity
import io.reactivex.Single
import javax.inject.Inject

class MusicItunesRemoteSourceImpl @Inject constructor(private val apiServices: AppleApiServices) :
    MusicItunesRemoteSource {
    override fun getSongList(
        term: String,
        country: String,
        media: String
    ): Single<BaseResponse<List<MusicNetworkEntity>>> {
        return apiServices.getSongList(
            term = term,
            country = country,
            media = media
        )
    }


}
