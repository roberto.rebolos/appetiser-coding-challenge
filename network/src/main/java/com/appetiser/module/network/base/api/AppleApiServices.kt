package com.appetiser.module.network.base.api

import com.appetiser.module.network.base.response.BaseResponse
import com.appetiser.module.network.features.music.model.MusicNetworkEntity
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface AppleApiServices {
    // searching three values in the remote via @Query and @GET Http methods
    @GET("search")
    fun getSongList(
        @Query("term") term: String,
        @Query("country") country: String,
        @Query("media") media: String,
    ): Single<BaseResponse<List<MusicNetworkEntity>>>

}