package com.appetiser.module.network

import com.appetiser.module.network.base.api.AppleApiServices
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ApiServiceModule {

    @Singleton
    @Provides
    fun provideAppleApi(retrofit: Retrofit): AppleApiServices =
        retrofit.create(AppleApiServices::class.java)
}
