package com.appetiser.module.network.features.music
import com.appetiser.module.network.base.response.BaseResponse
import com.appetiser.module.network.features.music.model.MusicNetworkEntity
import io.reactivex.Single


interface MusicItunesRemoteSource {
    fun getSongList(
        term: String,
        country: String,
        media: String,
    ): Single<BaseResponse<List<MusicNetworkEntity>>>
}