package com.appetiser.module.network

import com.appetiser.module.network.base.api.AppleApiServices
import com.appetiser.module.network.features.music.MusicItunesRemoteSource
import com.appetiser.module.network.features.music.MusicItunesRemoteSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RemoteSourceModule {

    @Provides
    @Singleton
    fun providesMusicRepository(appleApiServices: AppleApiServices):MusicItunesRemoteSource =
        MusicItunesRemoteSourceImpl(appleApiServices)
}
