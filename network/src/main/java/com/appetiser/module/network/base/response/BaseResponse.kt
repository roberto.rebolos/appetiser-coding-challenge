package com.appetiser.module.network.base.response

/**
 * @param T- is the model for response and reusable purposes
 * BaseResponse from itunes api
 */
data class BaseResponse<T>(
    val resultCount: Int,
    val results: T,
)
