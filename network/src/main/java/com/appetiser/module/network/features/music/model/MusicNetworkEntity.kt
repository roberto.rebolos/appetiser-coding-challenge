package com.appetiser.module.network.features.music.model

import com.google.gson.annotations.SerializedName

data class MusicNetworkEntity(
    @SerializedName("trackId") val id: Long = 0,
    @SerializedName("trackName") val name: String,
    @SerializedName("artworkUrl100") val imageURL: String,
    @SerializedName("trackPrice") val price: Float,
    @SerializedName("primaryGenreName") val genre: String,
    @SerializedName("longDescription") val description: String?,
    @SerializedName("maturityText") val maturity: String,
    @SerializedName("artistName") val artistName: String,

    )