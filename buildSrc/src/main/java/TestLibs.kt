object TestLibs {
    //Test Libs
    val androidXJUnit = "androidx.test.ext:junit:${Versions.androidXJunitVersion}"
    val archCoreTesting = "androidx.arch.core:core-testing:${Versions.archCompTestVersion}"
    val espresso = "androidx.test.espresso:espresso-core:${Versions.espressoVersion}"

    val junit = "junit:junit:${Versions.junitVersion}"
    val mockito = "org.mockito:mockito-inline:${Versions.mockitoVersion}"
    val testRules = "androidx.test:rules:${Versions.testRulesVersion}"
    val testRunner = "androidx.test:runner:${Versions.testRunnerVersion}"
    val espressoContri = "androidx.test.espresso:espresso-contrib:${Versions.espressoContrib}"
    val fragmentKtx = "androidx.fragment:fragment-ktx:${Versions.fragmentVersion}"
    val navigationTesting ="androidx.navigation:navigation-testing:${Versions.navigationTesting}"
    val coreKtx = "androidx.test:core-ktx:${Versions.fragmentVersion}"
    val fragmentTestingKtx = "androidx.fragment:fragment-testing:${Versions.fragmentVersion}"
    val truth = "com.google.truth:truth:${Versions.truthVersion}"

}